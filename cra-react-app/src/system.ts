
import fs from 'fs';
import { execSync } from 'child_process';
export default class System {
    static createFolder = (path: string) => {
        try {
            fs.mkdirSync(path, { recursive: true });
        } catch (error: any) {
            if (error.code !== 'EEXIST') throw error;
        }
    }

    static createFile = (path: string, content: string) => {
        try {
            fs.writeFileSync(path, content);
        } catch (error) {
            throw error;
        }
    }

    static execCommand = (command: string) => {
        try {
            const commandOutput = execSync(command);
            console.log(`Command output: ${commandOutput.toString()}`);
        } catch (error) {
            console.error(`Error executing the command: ${error}`);
        }
    }
}
