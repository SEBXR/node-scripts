const getUseLogicHookContent = () => {
    return "import { useState } from 'react';\n\n" +
        "const useLogic = () => {\n" +
        "  const [count, setCount] = useState(0);\n\n" +
        "  const increment = () => {\n" +
        "    setCount(prevCount => prevCount + 1);\n" +
        "  };\n\n" +
        "  const decrement = () => {\n" +
        "    setCount(prevCount => prevCount - 1);\n" +
        "  };\n\n" +
        "  return {\n" +
        "    count,\n" +
        "    increment,\n" +
        "    decrement\n" +
        "  };\n" +
        "};\n\n" +
        "export default useLogic;";
}

const getUseLoaderHookContent = () => {
    return "import { useState } from \"react\";" +
        "\n\n" +
        "const useLoader = (dataLoader: () => Promise<void | any>) => {" +
        "\n    const [loading, setLoading] = useState(false);" +
        "\n    const [hasError, setHasError] = useState(false);" +
        "\n    const [errorMessage, setErrorMessage] = useState(\"\");" +
        "\n\n    const loadData = async () => {" +
        "\n        try {" +
        "\n            setLoading(true);" +
        "\n            await dataLoader();" +
        "\n            setLoading(false);" +
        "\n        } catch (error: any) {" +
        "\n            setHasError(true);" +
        "\n            setErrorMessage(error.toString());" +
        "\n            setLoading(false);" +
        "\n        }" +
        "\n    }" +
        "\n\n    return{" +
        "\n        loading," +
        "\n        hasError," +
        "\n        errorMessage," +
        "\n        loadData" +
        "\n    }" +
        "\n}" +
        "\n\nexport default useLoader;";

}

const getUseFormHandlerHookContent = () => {
    return 'import { useState } from "react";\n\n' +
        'const useFormHandler = (formSubmitter: (e: React.FormEvent<HTMLFormElement>) => Promise<string | any>) => {\n' +
        '    const [loading, setLoading] = useState(false);\n' +
        '    const [hasError, setHasError] = useState(false);\n' +
        '    const [errorMessage, setErrorMessage] = useState("");\n\n' +
        '    const submit = async (e: React.FormEvent<HTMLFormElement>) => {\n' +
        '        try {\n' +
        '            setLoading(true);\n' +
        '            await formSubmitter(e);\n' +
        '            setLoading(false);\n' +
        '        } catch (error: any) {\n' +
        '            setHasError(true);\n' +
        '            setErrorMessage(error.toString());\n' +
        '            setLoading(false);\n' +
        '        }\n' +
        '    }\n\n' +
        '    return {\n' +
        '        loading,\n' +
        '        setLoading,\n' +
        '        hasError,\n' +
        '        errorMessage,\n' +
        '        submit\n' +
        '    }\n' +
        '}\n\n' +
        'export default useFormHandler;';
}

const getUseNetworkHookContent = () => {
    return 'import { Network } from "@capacitor/network";\n' +
        'import { useEffect, useState } from "react";\n\n' +
        'export const useNetwork = () => {\n\n' +
        '    const [networkStatus, setNetworkStatus] = useState<boolean | undefined>(undefined);\n\n' +
        '    useEffect(() => {\n' +
        '        getNetworkStatus();\n' +
        '        Network.addListener(\'networkStatusChange\', state => {\n' +
        '            setNetworkStatus(state.connected);\n' +
        '        });\n' +
        '    }, [networkStatus]);\n\n' +
        '    const getNetworkStatus = async (): Promise<void> => {\n' +
        '        const status = (await Network.getStatus()).connected;\n' +
        '        setNetworkStatus(status);\n' +
        '    }\n\n' +
        '    return networkStatus;\n\n' +
        '}\n\n' +
        'export default useNetwork;';
}


export { getUseLogicHookContent, getUseLoaderHookContent, getUseFormHandlerHookContent , getUseNetworkHookContent};
