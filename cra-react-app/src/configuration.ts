import System from "./system";
import fs from 'fs';

export default class Configuration {
    private static getBaseApiContent = () => {
        return "import axios, { AxiosInstance } from 'axios';\n" +
            "import { api } from '../utils/constant';\n" +
            "\n" +
            "class BaseApi {\n" +
            "  client: AxiosInstance;\n" +
            "  model: string;\n" +
            "\n" +
            "  constructor(_model: string) {\n" +
            "    this.client = axios.create({\n" +
            "      baseURL: `${api}`,\n" +
            "    });\n" +
            "    this.model = _model;\n" +
            "  }\n" +
            "\n" +
            "  async get(): Promise<any[]> {\n" +
            "    try {\n" +
            "      const response = await this.client.get(`${this.model}`);\n" +
            "      const data = await response.data;\n" +
            "      return data;\n" +
            "    } catch (error) {\n" +
            "      throw error;\n" +
            "    }\n" +
            "  }\n" +
            "\n" +
            "  async post(data: object): Promise<string> {\n" +
            "    try {\n" +
            "      const response = await this.client.post(`${this.model}`, data);\n" +
            "      return response.request.responseText;\n" +
            "    } catch (error) {\n" +
            "      throw error;\n" +
            "    }\n" +
            "  }\n" +
            "\n" +
            "  async put(data: { id: number, [key: string]: any }): Promise<string> {\n" +
            "    try {\n" +
            "      const response = await this.client.put(`${this.model}/${data.id}`, data);\n" +
            "      return response.request.responseText;\n" +
            "    } catch (error) {\n" +
            "      throw error;\n" +
            "    }\n" +
            "  }\n" +
            "\n" +
            "  async delete(id: number): Promise<string> {\n" +
            "    try {\n" +
            "      const response = await this.client.delete(`${this.model}/${id}`);\n" +
            "      return response.request.responseText;\n" +
            "    } catch (error) {\n" +
            "      throw error;\n" +
            "    }\n" +
            "  }\n" +
            "}\n" +
            "\n" +
            "export default BaseApi;";
    };


    private static getHomePageContent = (template?: string) => {
        if (template === '--ionic') {
            return `import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';\n\n` +
                `const Home: React.FC = () => {\n` +
                `    return (\n` +
                `        <IonPage>\n` +
                `            <IonHeader>\n` +
                `                <IonToolbar>\n` +
                `                    <IonTitle>Welcome!</IonTitle>\n` +
                `                </IonToolbar>\n` +
                `            </IonHeader>\n` +
                `            <IonContent className="ion-padding">\n` +
                `                <h2>This is an Ionic React App with a default architecture and the minimum dependencies installed.</h2>\n` +
                `            </IonContent>\n` +
                `        </IonPage>\n` +
                `    );\n` +
                `};\n\n` +
                `export default Home;`;
        }
        return "const Home = () => {\n" +
            "    return(\n" +
            "        <div>\n" +
            "            <h1>Welcome! </h1>\n" +
            "            <h2>This is a React App with a default architecture and the minimum dependencies installed.</h2>\n" +
            "        </div>\n" +
            "    )\n" +
            "}\n" +
            "export default Home;";
    }

    private static getAppRouterContent = (template?: string) => {
        if (template === '--ionic') {
            return `import { IonRouterOutlet } from "@ionic/react";\n` +
                `import { IonReactRouter } from "@ionic/react-router";\n` +
                `import { Route, Redirect } from "react-router";\n` +
                `import Home from "../pages/home/Home";\n\n` +

                `const AppRouter = () => {\n` +
                `    return (\n` +
                `        <IonReactRouter>\n` +
                `            <IonRouterOutlet>\n` +
                `                <Route exact path="/">\n` +
                `                    <Redirect to="/home" />\n` +
                `                </Route>\n` +
                `                <Route exact path="/home">\n` +
                `                    <Home />\n` +
                `                </Route>\n` +
                `            </IonRouterOutlet>\n` +
                `        </IonReactRouter>\n` +
                `    );\n` +
                `};\n\n` +

                `export default AppRouter;`;
        }
        return "import { Navigate, Route, Routes } from \"react-router-dom\";\n" +
            "import Home from \"../pages/home/Home\";\n" +
            "\n" +
            "const AppRouter = () => {\n" +
            "    return (\n" +
            "        <Routes>\n" +
            "            <Route path=\"/\" element={<Navigate to=\"/home\" />} />\n" +
            "            <Route path=\"/home\" element={<Home />} />\n" +
            "        </Routes>\n" +
            "    );\n" +
            "};\n" +
            "\n" +
            "export default AppRouter;"

    }

    private static getAppPageContent = (template?: string) => {
        if (template === '--ionic') {
            return `import { IonApp, setupIonicReact } from '@ionic/react';\n` +
                `import '@ionic/react/css/core.css';\n` +
                `import '@ionic/react/css/normalize.css';\n` +
                `import '@ionic/react/css/structure.css';\n` +
                `import '@ionic/react/css/typography.css';\n` +
                `import '@ionic/react/css/padding.css';\n` +
                `import '@ionic/react/css/float-elements.css';\n` +
                `import '@ionic/react/css/text-alignment.css';\n` +
                `import '@ionic/react/css/text-transformation.css';\n` +
                `import '@ionic/react/css/flex-utils.css';\n` +
                `import '@ionic/react/css/display.css';\n` +
                `import AppRouter from './routes/AppRouter';\n\n` +

                `setupIonicReact();\n\n` +

                `const App: React.FC = () => (\n` +
                `  <IonApp>\n` +
                `    <AppRouter />\n` +
                `  </IonApp>\n` +
                `);\n\n` +

                `export default App;`;
        }
        return "import './App.css';\n" +
            "import { BrowserRouter } from 'react-router-dom';\n" +
            "import AppRouter from './routes/AppRouter';\n" +
            "\n" +
            "function App() {\n" +
            "  return (\n" +
            "    <div className=\"App\">\n" +
            "      <BrowserRouter>\n" +
            "        <AppRouter />\n" +
            "      </BrowserRouter>\n" +
            "    </div>\n" +
            "  );\n" +
            "}\n" +
            "\n" +
            "export default App;";
    }

    static configureApisFolder = () => {
        try {
            System.createFile('.env', "VITE_API='your API endpoint'")
            System.createFile('src/utils/constant.ts', "export const api = import.meta.env.VITE_API;");
            System.createFile('src/apis/baseApi.ts', this.getBaseApiContent());
        } catch (error) {
            throw error;
        }
    }

    static configureHomePage = (template?: string) => {
        try {
            System.createFolder('src/pages/home');
            System.createFile('src/pages/home/Home.tsx', this.getHomePageContent(template));
            if (template === '--ionic') {
                System.execCommand('rm ./src/pages/Home.tsx ./src/pages/Home.css ./src/components/ExploreContainer.tsx ./src/components/ExploreContainer.css')
            }
        } catch (error) {
            throw error;
        }
    }

    static configureAppRouter = (template?: string) => {
        try {
            System.createFile('src/routes/AppRouter.tsx', this.getAppRouterContent(template));
            fs.writeFileSync('src/App.tsx', this.getAppPageContent(template), 'utf-8');
        } catch (error) {
            throw error;
        }
    }
}
