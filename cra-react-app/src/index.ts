#!/usr/bin/env ts-node

import path from "path";
import Configuration from "./configuration";
import System from "./system";

const createReactApp = () => {
    try {
        let projectName = process.argv[2] && process.argv[2] !== '--ionic' ? process.argv[2] : 'react-app';
        projectName = projectName.toLowerCase();
        const template = process.argv[process.argv.length - 1];
        if (template.startsWith('-') && template !== '--ionic') throw new Error('Unkown parameter !');
        if (template === '--ionic') {
            console.log('Generating the Ionic App ...');
            projectName = projectName !== 'react-app' ? projectName : 'ionic-app';
            System.execCommand(`ionic start ${projectName} blank --type=react --no-interactive`)
            console.log('Done !');
        }
        else
            System.execCommand(`npm create vite@latest ${projectName} -- --template react-ts`);
        process.chdir(projectName);
        System.execCommand("npm i && npm i axios react-router react-router-dom");
        System.createFolder('src/apis');
        System.createFolder('src/components');
        System.createFolder('src/hooks');
        System.createFolder('src/layouts');
        System.createFolder('src/pages');
        System.createFolder('src/routes');
        System.createFolder('src/types/interfaces');
        System.createFolder('src/utils');
        Configuration.configureApisFolder();
        Configuration.configureHomePage(template);
        Configuration.configureAppRouter(template);
        const projectType = template === '--ionic' ? 'Ionic React App' : 'React App';
        console.log(`Created ${projectType} structure in ${path.resolve(projectName)}`);
    } catch (error: any) {
        console.error(error.toString());
    }
}

createReactApp();
