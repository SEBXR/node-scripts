#!/usr/bin/env ts-node

import fs from 'fs';
import path from 'path';
import System from './system';
import { getUseLogicHookContent, getUseLoaderHookContent, getUseFormHandlerHookContent, getUseNetworkHookContent } from './hookContent';

const searchFilePath = (dir: string, fileName: string): string => {
    const stack: string[] = [dir];
    while (stack.length > 0) {
        const currentDir: string = stack.pop()!;
        const files: string[] = fs.readdirSync(currentDir);
        for (const file of files) {
            const filePath: string = path.join(currentDir, file);
            const fileStat: fs.Stats = fs.statSync(filePath);
            if (fileStat.isDirectory()) {
                stack.push(filePath);
            } else if (file === fileName) {
                return currentDir + '/';
            }
        }
    }
    throw new Error('Component not found !')
};


const generate = () => {
    try {
        const hook = process.argv[2];
        if (!hook) throw new Error('Please provide a hook name');
        switch (hook) {
            case 'useLogic':
                const component = process.argv[3];
                if (!component) throw new Error('Please provide a component');
                const filePath = searchFilePath('./src/', component + '.tsx');
                System.createFile(`${filePath}useLogic.ts`, getUseLogicHookContent());
                console.log(`useLogic hook for ${component} component is created`);
                break;
            case 'useLoader':
                System.createFile(`./src/hooks/useLoader.ts`, getUseLoaderHookContent());
                console.log(`useLoader hook is created`);
                break;
            case 'useFormHandler':
                System.createFile(`./src/hooks/useFormHandler.ts`, getUseFormHandlerHookContent());
                console.log(`useFormHandler hook is created`);
                break;
            case 'useNetwork':
                System.execCommand('npm i @capacitor/core')
                System.execCommand('npm i -D @capacitor/cli')
                System.execCommand('npm i @capacitor/network');
                System.createFile(`./src/hooks/useNetwork.ts`, getUseNetworkHookContent());
                break;
            default:
                console.error('Please provide an existing hook : useLogic , useLoader , useFormHandler , or useNetwork')
        }

    } catch (error: any) {
        console.error(error.toString());
    }
}

generate();


