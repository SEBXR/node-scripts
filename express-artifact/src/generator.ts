import System from "./system";
import _ from 'lodash';

export type commands = 'controller' | 'model' | 'service' | 'route' | 'all';

const str = _;
export default class Generator {

    name: string;

    constructor(name: string) {
        this.name = name;
    }

    checkErrors = (command: string) => {
        const usableCommands = ['model', 'controller', 'service', 'route', 'all'];
        if (!command)
            throw new Error('Please provide a command !');
        if (!usableCommands.includes(command))
            throw new Error('Please provide a valid command !');
        if (command && !this.name)
            throw new Error(`Please provide a ${command} name!`);
        if (process.argv[4])
            throw new Error('Too much arguments !')
    }

    model = () => {
        try {
            if (!System.checkDir('src/models')) System.createFolder('src/models');
            System.createFile(`src/models/${str.lowerFirst(this.name)}.ts`, `export default class ${str.upperFirst(this.name)} { \n\n}`);

        } catch (error) {
            throw error;
        }
    }

    controller = () => {
        try {
            if (!System.checkDir('src/controllers')) System.createFolder('src/controllers');
            System.createFile(`src/controllers/${str.lowerFirst(this.name)}Controller.ts`, `export default class ${str.upperFirst(this.name)}Controller { \n\n}`);
        } catch (error) {
            throw error;
        }
    }


    service = () => {
        try {
            if (!System.checkDir('src/services')) System.createFolder('src/services');
            System.createFile(`src/services/${str.lowerFirst(this.name)}Service.ts`, `export default class ${str.upperFirst(this.name)}Service { \n\n}`);
        } catch (error) {
            throw error;
        }
    }

    route = () => {
        try {
            if (!System.checkDir('src/routes')) System.createFolder('src/routes');
            System.createFile(`src/routes/${str.lowerFirst(this.name)}Router.ts`, `import express from 'express';\n\nconst ${str.lowerFirst(this.name)}Router = express.Router();\n\nexport default ${str.lowerFirst(this.name)}Router;`);
        } catch (error) {
            throw error;
        }
    }

    all = () => {
        try {
            this.model();
            this.controller();
            this.service();
            this.route();
        } catch (error) {
            throw error;
        }
    }

}
