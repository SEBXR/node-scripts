#!/usr/bin/env ts-node

import _ from "lodash";
import Generator, { commands } from "./generator";

const generate = (): string => {
    let output = '';
    try {
        const command = process.argv[2] as commands;
        const name = process.argv[3];
        const generator = new Generator(name);
        generator.checkErrors(command);
        generator[command]();
        if (command === 'all')
            output = `The model,controller,service and route are created successfully !`;
        else
            output = `${_.upperFirst(command)} created successfully !`;
        console.log(output);
    } catch (error: any) {
        console.error(error.toString());
    }
    return output;
}

generate();
