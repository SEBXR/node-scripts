# Create a new express project using the cra-express-app

### If not installed , please refer to this documentation
[cra-express-app](https://gitlab.com/SEBXR/node-scripts/-/tree/main/cra-express-app?ref_type=heads)

### Linux
**cra-express-app *app_name(optional)***
### Windows
**ts-node-cwd.cmd $env:cra_express_app *app_name(optional)***


# Linux Setup
### 1. Make src/index.ts executable
**chmod +x express-artifact/src/index.ts**

### 2. Add alias to .bashrc (Modify path)
**echo "alias express-artifact='/path/to/express-artifact/src/index.ts'" >> ~/.bashrc**
**source ~/.bashrc**

### 3. Running the script
**In the express project(root folder) run:**

**express-artifact *command* *name***

**The possible command are: *model , controller , service , route or all.***

# Windows Setup
### 1. Create environment variable
**setx express_artifact "/path/to/cra-express-app/src/index.ts"**

### 2. Running the script
**In the express project(root folder) run:**

**ts-node-cwd.cmd $env:express_artifact [command] [name]**

**The possible command are: *model , controller , service , route or all.***
