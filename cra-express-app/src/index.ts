#!/usr/bin/env ts-node

import path from 'path';
import System from './system';
import Configuration from './configuration';


const createExpressApp = () => {
    try {
        let projectName = process.argv[2] || 'express-app';
        projectName = projectName.toLowerCase();
        System.createFolder(projectName);
        process.chdir(projectName);
        System.execCommand('npm init -y');
        Configuration.configurePackageJSON(projectName);
        System.execCommand('npm run init-ts');
        Configuration.configureTSconfig();
        System.execCommand('npm i express dotenv ts-node-dev typescript cors');
        System.execCommand('npm i @types/express @types/cors');
        System.createFolder('dist');
        System.createFolder('src');
        System.createFolder('src/controllers');
        System.createFolder('src/services');
        System.createFolder('src/models');
        System.createFolder('src/routes');
        System.createFolder('src/uploads');
        System.createFolder('src/utils');
        System.createFile('.env', 'PORT=3000');
        System.createFile('src/routes/routes.ts', Configuration.getRouteFileContent());
        System.createFile('src/index.ts', Configuration.getIndexFileContent());
        console.log(`Created Express App structure in ${path.resolve(projectName)}`);

    } catch (error: any) {
        console.error(error.toString());
    }
}

createExpressApp();

