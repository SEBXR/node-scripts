import fs from 'fs';

export default class Configuration {
    static configurePackageJSON = (projectName: string) => {
        try {
            const packageJSON = require('./../package.json');
            packageJSON.name = projectName;
            packageJSON.main = 'src/index.ts';
            packageJSON.scripts = {
                "start": "ts-node-dev src/index.ts",
                "build": "tsc -p tsconfig.json",
                "init-ts": "tsc --init"
            };

            fs.writeFileSync('./package.json', JSON.stringify(packageJSON, null, 2));
            console.log('package.json is configured correctly !');
        } catch (error) {
            throw error;
        }
    }

    static configureTSconfig = () => {
        try {
            const TSconfig = require('./../tsconfig.json');
            TSconfig.compilerOptions.outDir = "./dist";
            const regexPattern = /\/\*[\s\S]*?\*\//g;
            fs.writeFileSync('./tsconfig.json', JSON.stringify(TSconfig, null, 2).replace(regexPattern, ''));
            console.log('tsconfig.json is configured correctly !');
        } catch (error) {
            throw error;
        }
    }

    static getRouteFileContent = () => {
        return `import express from 'express';
        \nconst router = express.Router();
        \nrouter.get('/', (req, res) => {\n  res.send('Hello, API!');\n});
        \nexport default router;
        `
    }

    static getIndexFileContent = () => {
        return `import express from 'express';
import router from './routes/routes';
import dotenv from 'dotenv';
import cors from 'cors';
        \ndotenv.config();
const port = process.env.PORT || 3000;
const app = express();
\napp.use(express.json());
app.use(cors());
\napp.get('/', (req, res) => {\n  res.send('Hello, World!');\n});
        \napp.use('/api', router);
        \napp.listen(port, () => console.log('Server is running on http://localhost:' + port + '/'));
        `
    }
}
