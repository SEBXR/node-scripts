# Install dependencies (I assume you already have node and npm) (both Windows & Linux)
**npm i -g ts-node**

# Linux Setup
### 1. Make src/index.ts executable
**chmod +x cra-express-app/src/index.ts**

### 2. Add alias to .bashrc (Modify path)
**echo "alias cra-express-app='/path/to/cra-express-app/src/index.ts'" >> ~/.bashrc**
**source ~/.bashrc**

### 3. Running the script
**cra-express-app *app_name(optional)***

# Windows Setup
### 1. Create environment variable
**setx cra_express_app "/path/to/cra-express-app/src/index.ts"**

### 2. Running the script
**ts-node-cwd.cmd $env:cra_express_app *app_name(optional)***
